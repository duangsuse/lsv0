package org.duangsuse.ls;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareAssets();
        LinearLayout layout_main = new LinearLayout(this);
        final EditText output = new EditText(this); 
        Button btn_run = new Button(this);
        
        btn_run.setOnClickListener(new Button.OnClickListener() {
            @Override public void onClick(View v) {
                output.setText(execute(getPkgFilesPath()+ "ls /sdcard/ls/"));
            }
        });
    }

    void prepareAssets() {
        if (! (execute("ls" + getPkgFilesPath()).contains("scanutil"))) {
            copyAssetFile("ls", getPkgFilesPath(), "ls");
        }
    }

    String execute(String arg) {
        Runtime runtime = Runtime.getRuntime();
        Process ls = null;
        try {
            ls = runtime.exec(arg);
        } catch (Throwable e) {
            Toast.makeText(this, e.getStackTrace().toString(), Toast.LENGTH_LONG).show();
        }
        int exit_code = 0; //useless unused variable
        try {
            exit_code = ls.waitFor();
        } catch (Throwable e) {} // :pill: but powerful #(funny)
        BufferedReader buf_in = new BufferedReader(
            new InputStreamReader(ls.getInputStream()));
        StringBuffer stringBuf = new StringBuffer();
        String line = null;
        try {
                while ((line = buf_in.readLine()) != null) {
                    stringBuf.append(line);
                }
            } catch (Throwable e) {}
        return stringBuf.toString();
    }

    String getPkgFilesPath() {
        return String.format("/data/data/%1$s/files/", getPkgName());
    }

    String getPkgName() {
        return this.getClass().getPackage().getName(); // :-(
    }

    // copycat duangsuse :cat:
    void copyAssetFile(String assetName, String savePath, String saveName) {
        String filename = savePath + "/" + saveName; // /////root == /root :+1:
        File dir = new File(savePath);
        if (!dir.exists())
            dir.mkdir();
        try {
            if (!(new File(filename)).exists()) {
                InputStream is = getResources().getAssets().open(assetName);
                FileOutputStream fos = new FileOutputStream(filename);
                byte[] buffer = new byte[7168];
                int count = 0;
                while ((count = is.read(buffer)) > 0) {
                fos.write(buffer, 0, count);
            }
            fos.close();
            is.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
